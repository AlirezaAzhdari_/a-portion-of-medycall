const kavenegar = require("../services/Kavenegar/index");
const Agenda = require("agenda");
const agenda = new Agenda();

module.exports.jobs = async (agenda) => {
  agenda.define("sendSchedulingSMS", async (job) => {
    const jobData = job.attrs.data;
    const data = {
      message: jobData.message,
      phoneNumber: jobData.phoneNumber,
    };

    const response = await kavenegar.schedulingSMS.sendSMS(data);

    if (response.result !== "Ok") {
      return { error: "پیامک ارسال نشد" };
    }
  });
};

module.exports.agenda = agenda;
