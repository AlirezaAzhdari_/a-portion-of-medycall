const mongoose = require("mongoose");
const Joi = require("joi");
const jwt = require("jsonwebtoken");
const config = require("../config");
const services = require("../services");
const isEmpty = require("../loaders/middlewares/isEmpty");

const doctorSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 2,
    maxlength: 255,
  },
  phoneNumber: {
    type: String,
    required: true,
    minlength: 11,
    maxlength: 12,
  },
  biography: {
    type: String,
    maxlength: 1000,
  },
  avatarUrl: {
    type: String,
  },
  videoClipUrl: [
    {
      type: String,
    },
  ],
  gender: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
  },
  platform: {
    type: String,
    default: "web",
  },
  categoryName: {
    required: true,
    type: String,
  },
  doctorType: {
    type: Number,
    default: 1,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 256,
  },
  nezamPezeshki: {
    type: Number,
    unique: true, //required
  },
  numberOfSessions: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

function validateDoctor(doctor) {
  const scheme = {
    phoneNumber: Joi.string().min(11).max(12).required(),
    password: Joi.string().required().min(8).max(256),
  };

  return Joi.validate(doctor, scheme);
}

function emptyCheck(doctor) {
  if (isEmpty(doctor.phoneNumber)) {
    return {
      empty: "yes",
      message: services.apiMessages.messages.authentication.phoneEmpty,
    };
  }

  if (isEmpty(doctor.password)) {
    return {
      empty: "yes",
      message: services.apiMessages.messages.authentication.passwordEmpty,
    };
  }

  return { empty: "no", message: "" };
}

doctorSchema.methods.generateAuthToken = function () {
  const token = jwt.sign(
    {
      _id: this._id,
    },
    config.jwt.JWT_WEBTOKEN
  );
  return token;
};

const Doctor = mongoose.model("doctor", doctorSchema);

exports.Doctor = Doctor;
exports.validate = validateDoctor;
exports.emptyCheck = emptyCheck;
