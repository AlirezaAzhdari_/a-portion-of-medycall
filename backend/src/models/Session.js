const mongoose = require("mongoose");

const sessionSchema = new mongoose.Schema({
  messagesCount: {
    type: Number,
    default: 0,
  },
  patient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "patient",
  },
  doctor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "doctor",
  },
  payment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "payment",
  },
  patientInitialRecord: {
    age: {
      type: String,
    },
    description: {
      type: String,
    },
    gender: {
      type: String,
    },
  },
  date: {
    type: Date,
    required: true,
  },
  tag: {
    startHour: {
      type: Number,
      required: true,
    },
    startMinute: {
      type: Number,
      require: true,
    },
    duration: {
      type: Number,
      require: true,
    },
  },
  status: {
    type: String,
    default: "pending",
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const Session = mongoose.model("session", sessionSchema);
module.exports.Session = Session;
