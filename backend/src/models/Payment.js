const mongoose = require("mongoose");
const paymentSchema = new mongoose.Schema({
  patient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "patient",
  },
  couponID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "coupon",
  },
  amount: {
    type: Number,
    required: true,
  },
  authority: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const Payment = mongoose.model("payment", paymentSchema);
module.exports.Payment = Payment;
