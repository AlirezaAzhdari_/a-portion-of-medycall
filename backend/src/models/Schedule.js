const mongoose = require("mongoose");

const scheduleSchema = new mongoose.Schema({
  doctor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "doctor",
  },
  schedules: [
    {
      date: {
        type: Date,
        required: true,
      },
      tags: [
        {
          startHour: {
            type: Number,
            required: true,
          },
          endHour: {
            type: Number,
            required: true,
          },
          position: {
            type: Number,
            default: 0,
          },
        },
      ],
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const Schedule = mongoose.model("schedule", scheduleSchema);
exports.Schedule = Schedule;
