const mongoose = require("mongoose");
const profileSchema = new mongoose.Schema({
  firstName: {
    type: String,
    minlength: 2,
    maxlength: 255,
  },
  lastName: {
    type: String,
    minlength: 2,
    maxlength: 255,
  },
  phoneNumber: {
    type: String,
    required: true,
    minlength: 11,
    maxlength: 12,
  },
  biography: {
    type: String,
    maxlength: 1000,
  },
  avatarUrl: {
    type: String,
  },
  gender: {
    type: String,
  },
  age: {
    type: Number,
  },
  platform: {
    type: String,
    default: "web",
  },
  notificationToken: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const Profile = mongoose.model("profile", profileSchema);
module.exports.Profile = Profile;
