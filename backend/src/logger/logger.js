const winston = require("winston");

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      prettyPrint: true,
    }),
    new winston.transports.File({
      filename: "./src/logger/log_files/combined.log",
    }),
  ],
  exceptionHandlers: [
    new winston.transports.File({
      filename: "./src/logger/log_files/uncaughtExceptions.log",
    }),
  ],
});

module.exports = { logger };
