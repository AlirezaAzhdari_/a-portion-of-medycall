const express = require("express");
const router = express.Router();
const services = require("../../services");
const asyncMiddleware = require("../../loaders/middlewares/async");
const auth = require("../../loaders/middlewares/auth");

//Get a doctor's schedule
router.get(
  "/get-doctor-schedule",
  auth,
  asyncMiddleware(async (req, res) => {
    const { result, error } = await services.schedules.getSchedule.byDoctorId({
      doctor_id: req.auth._id,
    });

    if (error) {
      return res.status(404).json({ error });
    }

    res.status(200).json({ result });
  })
);

module.exports = router;
