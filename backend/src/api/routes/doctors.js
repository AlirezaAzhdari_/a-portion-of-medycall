const express = require("express");
const bcrypt = require("bcrypt");
const router = express.Router();
const services = require("../../services");
const { Doctor } = require("../../models/Doctor");
const { validate, emptyCheck } = require("../../models/Doctor");
const auth = require("../../loaders/middlewares/auth");
const asyncMiddleware = require("../../loaders/middlewares/async");

//Post create a new doctor : Admin
router.post(
  "/create-doctor",
  asyncMiddleware(async (req, res) => {
    const { doctor, error } = await services.doctor.createDoctor(req.body);

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ doctor });
  })
);

//Get current doctor
router.get(
  "/current-doctor",
  auth,
  asyncMiddleware(async (req, res) => {
    const doctor = await Doctor.findById(req.auth._id);

    if (!doctor) {
      return res.status(404).json({ error: "پزشک یافت نشد" });
    }

    res.status(200).json({ doctor });
  })
);

//Get all doctors
router.get(
  "/get-all",
  asyncMiddleware(async (req, res) => {
    const { doctors, error } = await services.doctor.getDoctor.all();

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ doctors });
  })
);

//Get Doctors by search
router.post(
  "/get-full-info-search",
  asyncMiddleware(async (req, res) => {
    const { fullDoctors, error } = await services.doctor.allInfoSearch(
      req.body
    );

    if (error) {
      return res.status(400).json({ error });
    }

    res.status(200).json({ fullDoctors });
  })
);

//Get a doctor by ID
router.get(
  "/get-by-id/:id",
  asyncMiddleware(async (req, res) => {
    const { doctor, error } = await services.doctor.getDoctor.byID(
      req.params.id
    );

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ doctor });
  })
);

//Get doctors by category
router.get(
  "/get-by-category",
  asyncMiddleware(async (req, res) => {
    const { doctors, error } = await services.doctor.getDoctor.byCategory(
      req.body
    );

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ doctors });
  })
);

//Post login
router.post(
  "/login",
  asyncMiddleware(async (req, res) => {
    const { empty, message } = emptyCheck(req.body);
    if (empty === "yes") {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ error: message });
    }

    const { error } = validate(req.body);

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({
          error: services.apiMessages.messages.authentication.somethingIsWrong,
        });
    }

    const doctor = await Doctor.findOne({
      phoneNumber: req.body.phoneNumber,
    });

    if (!doctor) {
      return res.status(services.apiMessages.status.asyncStatus.notFound).json({
        error: services.apiMessages.messages.authentication.userNotFound,
      });
    }

    const result = await bcrypt.compare(req.body.password, doctor.password);

    if (result) {
      const token = doctor.generateAuthToken();
      return res
        .status(services.apiMessages.status.asyncStatus.ok)
        .header("x-auth-token", token)
        .json({ doctor, token });
    }

    res.status(services.apiMessages.status.asyncStatus.notFound).json({
      message: services.apiMessages.messages.authentication.passwordIsWrong,
    });
  })
);

//Post set the doctor's scheduling
router.post(
  "/set-scheduling",
  auth,
  asyncMiddleware(async (req, res) => {
    if (!req.body.schedule) {
      return res.status(400).json({ error: "دوباره تلاش کنید" });
    }

    const { error } = await services.doctor.scheduling({
      doctor_id: req.auth._id,
      schedule: req.body.schedule,
    });

    if (error) {
      return res.status(400).json({ error });
    }

    res.status(200).json({ error: null });
  })
);

router.delete(
  "/reset-schedule",
  asyncMiddleware(async (req, res) => {
    const { message, error } = await services.doctor.editDoctor.resetSchedule(
      req.body
    );

    if (error) {
      return res.status(400).json({ error });
    }

    res.status(200).json({ message });
  })
);

module.exports = router;
