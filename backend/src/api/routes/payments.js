const express = require("express");
const router = express.Router();
const { Coupon } = require("../../models/Coupon");
const services = require("../../services");
const asyncMiddleware = require("../../loaders/middlewares/async");
const calculatePrice = require("../../loaders/utils/calculatePrice");
const {
  zarinpal,
} = require("../../services/Payment/payment/initializeZarinpal");

// Create a new payment
router.post(
  "/create-payment",
  asyncMiddleware(async (req, res) => {
    const { payment } = await services.payment.createPayment(req.body);
    res.status(services.apiMessages.status.asyncStatus.ok).json({ payment });
  })
);

//Get calculate the price after a coupon takes effect.
router.post(
  "/calculate-price",
  asyncMiddleware(async (req, res) => {
    const coupon = await Coupon.findOne({ name: req.body.couponName });
    if (!coupon) {
      return res.status(400).json({ error: "کد تخفیف نامعتبر" });
    }

    const newPrice = calculatePrice({
      price: req.body.price,
      discount: coupon.discount,
    });

    coupon.count--;
    await coupon.save();

    res.status(200).json({ newPrice });
  })
);

//Get request for a new payment
router.post(
  "/payment-request",
  asyncMiddleware(async (req, res) => {
    const response = await services.payment.initializeZarinpal.zarinpal.PaymentRequest(
      {
        Amount: req.body.amount ? req.body.amount : "30000",
        CallbackURL: req.body.callbackURL
          ? req.body.callbackURL
          : "https://medycall.com",
        Description: "Medycall Payment API",
        Email: req.body.email ? req.body.email : "",
        Mobile: req.body.phoneNumber ? req.body.phoneNumber : "",
      }
    );
    console.log(response);
    if (response.status === 100) {
      res.redirect(response.url);
    }
  })
);

//Get Verifu the payment and create a payment for the corresponsive patient
router.get(
  "/payment-verification/:amount/:token",
  asyncMiddleware(async (req, res) => {
    const response = await zarinpal.PaymentVerification({
      Amount: req.params.amount,
      Authority: req.params.token,
    });

    if (response.status == 101) {
      //تایید پرداخت و ساخت یک پیمنت برای بیمار
      console.log("Verified! Ref ID: " + response.RefID);
      const { payment } = await services.payment.createPayment({
        patient_id: req.body.patient_id,
        coupon_id: req.body.coupon_id,
        amount: req.params.amount,
        authority: req.params.authority,
      });
      res.status(services.apiMessages.status.asyncStatus.ok).json({ payment });
    } else {
      //عدم تایید پرداخت
      console.log(response);
      res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ error: "پرداخت تایید نشد" });
    }
  })
);

//Get admin can get the unverified transactions
router.get(
  "unverified-transactions",
  asyncMiddleware(async (req, res) => {
    const response = await zarinpal.UnverifiedTransactions();
    if (response.status == 100) {
      console.log(response.authorities);
    } else {
      console.log(response);
    }
  })
);

module.exports = router;
