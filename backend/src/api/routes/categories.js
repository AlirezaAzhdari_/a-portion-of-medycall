const express = require("express");
const router = express.Router();
const services = require("../../services");
const asyncMiddleware = require("../../loaders/middlewares/async");
const { validate, emptyCheck } = require("../../models/Category");

//Post create a new category
router.post(
  "/create-category",
  asyncMiddleware(async (req, res) => {
    const { empty, _message } = emptyCheck(req.body);
    if (empty === "yes") {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ message: _message });
    }

    const { error: validationError } = validate(req.body);

    if (validationError) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({
          message: services.apiMessages.messages.category.somethingIsWrong,
        });
    }

    const { category, error } = await services.category.createCategory(
      req.body
    );

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.serverInternalError)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ category });
  })
);

//Post create multiple categories together

//Post a category by name
router.post(
  "/get-a-category",
  asyncMiddleware(async (req, res) => {
    if (!req.body.name) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ error: "Filed name can not be empty." });
    }

    const { category, error } = await services.category.getCategory.byName(
      req.body
    );
    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }
    res.status(services.apiMessages.status.asyncStatus.ok).json({ category });
  })
);

//Post categories of a certain price
router.post(
  "/get-categories-by-price",
  asyncMiddleware(async (req, res) => {
    if (!req.body.price) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ error: "Filed price can not be empty." });
    }

    const { categories, error } = await services.category.getCategory.byPrice(
      req.body
    );

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }
    res.status(services.apiMessages.status.asyncStatus.ok).json({ categories });
  })
);

//Get all categories
router.get(
  "/get-categories",
  asyncMiddleware(async (req, res) => {
    const { categories, error } = await services.category.getCategory.all();
    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ categories });
  })
);

//Put edit a category's price
router.put(
  "/edit-category-price",
  asyncMiddleware(async (req, res) => {
    if (!req.body.name || !req.body.newPrice) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ error: "Field name and new price can not be empty." });
    }

    const { category, error } = await services.category.editCategory.editPrice(
      req.body
    );

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({ error });
    }
    res.status(services.apiMessages.status.asyncStatus.ok).json({ category });
  })
);

module.exports = router;
