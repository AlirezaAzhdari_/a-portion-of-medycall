const express = require("express");
const router = express.Router();
const { Patient } = require("../../models/Patient");
const auth = require("../../loaders/middlewares/auth");
const services = require("../../services");

const asyncMiddleware = require("../../loaders/middlewares/async");

// Post create a new patient/ Sign up
router.post(
  "/create-patient/:step",
  asyncMiddleware(async (req, res) => {
    // TODO validate req.body fields (maybe)

    var step = req.params.step;
    if (!step) {
      step = "1";
    }

    if (step === "1") {
      if (!req.body.phoneNumber) {
        return res
          .status(services.apiMessages.status.asyncStatus.badRequest)
          .json({ error: ".شماره تلفن خود را وارد کنید" });
      }

      const { token, error } = await services.patient.createPatient.first(
        req.body
      );

      if (error) {
        return res
          .status(services.apiMessages.status.asyncStatus.serverInternalError)
          .json({
            error: services.apiMessages.messages.verification.error,
          });
      }
      return res
        .status(services.apiMessages.status.asyncStatus.ok)
        .json({ token: token });
    }

    const {
      patient,
      authToken,
      error,
    } = await services.patient.createPatient.second(req.body);

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.badRequest)
        .json({
          error: error,
        });
    }
    res
      .status(services.apiMessages.status.asyncStatus.ok)
      .header("x-auth-token", authToken)
      .json({ patient: patient, authToken: authToken });
  })
);

//Get current patient
router.get(
  "/current-patient",
  auth,
  asyncMiddleware(async (req, res) => {
    console.log(req.auth._id);
    const patient = await Patient.findById(req.auth._id);

    if (!patient) {
      return res.status(404).json({ error: "بیمار یافت نشد" });
    }

    res.status(200).json({ patient });
  })
);

//Get a patient by profile
router.get(
  "/get-patient-by-profile",
  asyncMiddleware(async (req, res) => {
    const { patient } = await services.patient.getAPatient.byProfile(req.body);
    if (!patient) {
      return res.json({
        error: services.apiMessages.messages.db.patientNotFound,
      });
    }

    res
      .status(services.apiMessages.status.asyncStatus.badRequest)
      .json({ patient });
  })
);

router.post(
  "/schedule-session",
  auth,
  asyncMiddleware(async (req, res) => {
    //!req.body.payment_id ||
    if (!req.body.doctor_id || !req.body.session) {
      return res.status(400).json({ error: "error" });
    }
    const data = {
      patient_id: req.auth._id,
      doctor_id: req.body.doctor_id,
      payment_id: "",
      session: req.body.session,
      doctorType: req.body.doctorType,
    };
    const { result, error } = await services.patient.scheduleSession(data);

    if (error) {
      return res.status(400).json({ error });
    }

    res.status(200).json({ session: result.session });
  })
);

module.exports = router;
