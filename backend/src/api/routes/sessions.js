const express = require("express");
const router = express.Router();
const services = require("../../services");
const { Session } = require("../../models/Session");
const { Patient } = require("../../models/Patient");
const { Doctor } = require("../../models/Doctor");
const auth = require("../../loaders/middlewares/auth");
const asyncMiddleware = require("../../loaders/middlewares/async");

// Create a new session with it's conversation
router.post(
  "/create-session",
  asyncMiddleware(async (req, res) => {
    //TODO validate req.body fields (maybe)
    const { session } = await services.session.createSession(req.body);
    res.status(services.apiMessages.status.asyncStatus.ok).json({ session });
    // Agenda
  })
);

//Post current session by patient and doctor
router.post(
  "/current-session",
  asyncMiddleware(async (req, res) => {
    const {
      session,
      error,
    } = await services.session.getASession.byPatientAndDoctor(req.body);

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ session });
  })
);

// Get doctor's all sessions
router.get(
  "/my-sessions-doctor",
  auth,
  asyncMiddleware(async (req, res) => {
    const {
      sessions,
      error,
    } = await services.session.getASession.allDoctorSessions({
      doctor_id: req.auth._id,
    });

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ sessions });
  })
);

// Get a session by ID
router.get(
  "/current-session/:id",
  asyncMiddleware(async (req, res) => {
    const { session, error } = await services.session.getASession.byID(
      req.params.id
    );

    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.notFound)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ session });
  })
);

// Upload an image
router.post(
  "/upload-image",
  asyncMiddleware(async (req, res) => {
    if (!req.body.session) {
      return res
        .status(400)
        .json({ error: "session id field should not be empty" });
    }
    const image = req.files.image;
    const data = { image: image.tempFilePath, session: req.body.session };

    const { success, error } = await services.cloudinary.uploadImage(data);
    if (success) {
      return res.json({
        message: services.apiMessages.messages.cloudinary.image.success,
      });
    }
    res.status(400).json({ error });
  })
);

// Make a voice call
router.post(
  "/make-voice-call",
  asyncMiddleware(async (req, res) => {
    if (!req.body.session || !req.body.doctor || !req.body.patient) {
      return res.status(400).json({
        error: `required fields should not be empty: session Id, doctor Id, patient Id`,
      });
    }
    const data = {
      session: req.body.session,
      caller: req.body.doctor,
      receptor: req.body.patient,
    };
    const { response, error } = await services.session.makeVoiceCall(data);
    if (error) {
      return res
        .status(services.apiMessages.status.asyncStatus.serverInternalError)
        .json({ error });
    }

    res.status(services.apiMessages.status.asyncStatus.ok).json({ response });
  })
);

module.exports = router;
