const config = require("../config");
const sessions = require("./routes/sessions");
const patients = require("./routes/patients");
const payments = require("./routes/payments");
const profiles = require("./routes/profiles");
const categories = require("./routes/categories");
const conversations = require("./routes/conversations");
const doctors = require("./routes/doctors");
const admins = require("./routes/admins");
const schedules = require("./routes/schedules");

module.exports = function (app) {
  app.use("/api/patients", patients);
  app.use("/api/sessions", sessions);
  app.use("/api/payments", payments);
  app.use("/api/profiles", profiles);
  app.use("/api/doctors", doctors);
  app.use("/api/categories", categories);
  app.use("/api/conversations", conversations);
  app.use("/api/admins", admins);
  app.use("/api/schedules", schedules);
};
