const express = require("express");
const cors = require("cors");
const cluster = require("cluster");
const fileupload = require("express-fileupload");
const cloudinary = require("cloudinary");
const firebaseAdmin = require("firebase-admin");
const bodyParser = require("body-parser");
const config = require("../config");
const services = require("../services");
const serviceAccountKey = require("../config/firebase/firebase-adminsdk.json");
const routes = require("../api");
const errors = require("./middlewares/error");
const { agenda, jobs } = require("../jobs");
const { logger } = require("../logger/logger");

const app = express();

module.exports.startup = async () => {
  if (cluster.isMaster) {
    var numberOfWorkers = require("os").cpus().length;
    for (let i = 0; i < numberOfWorkers; i++) {
      cluster.fork();
    }
    cluster.on("online", (worker) => {
      logger.info(`Worker ${worker.process.is} is online`);
    });
    cluster.on("exit", (worker, code, signal) => {
      logger.info(
        `Worker ${worker.process.id} died with code ${code} and signal ${signal}`
      );
      logger.info("Spawning a new worker");
      cluster.fork();
    });
  } else {
    app.use(cors());
    app.use(fileupload({ useTempFiles: true }));
    app.use(bodyParser.json());
    app.use(express.urlencoded({ extended: true }));
    routes(app);
    app.use(errors);

    try {
      const db = await services.database.connect();
      agenda.mongo(db, "jobs");

      agenda.on("ready", async () => {
        await agenda.start();
        console.log("agenda started");
      });

      await jobs(agenda);
    } catch (error) {
      console.log(error);
    }

    agenda.on("fail", function (err, job) {
      return console.log("Job failed with error: " + err.message);
    });

    cloudinary.config({
      cloud_name: config.cloudinary.CLOUD_NAME,
      api_key: config.cloudinary.API_KEY,
      api_secret: config.cloudinary.API_SECRET,
    });

    firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.cert(serviceAccountKey),
      databaseURL: config.firebase.databaseURL,
    });

    services.socket.initSocket(app);

    //Throw an exception on unhandled rejection
    process.on("unhandledRejection", (ex) => {
      throw ex;
    });

    server = app.listen(config.port, (err) => {
      if (err) {
        logger.error(err);
        return process.exit(1);
      }
      console.log(`
      ################################################
      🛡️  Process ${process.id} is listening on port: ${config.port} 🛡️
      ################################################`);
    });

    return server;
  }
};
