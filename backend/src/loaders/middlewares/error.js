const { logger } = require("../../logger/logger");
const services = require("../../services");

module.exports = function (err, req, res, next) {
  logger.error(err.message);
  console.log("Error middleware: " + err.message);
  res.status(500).send({
    message: services.apiMessages.messages.asyncErro.failed,
  });
};
