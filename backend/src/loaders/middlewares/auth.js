const jwt = require("jsonwebtoken");
const config = require("../../config");
const messages = require("../../services/API Messages/messages/messages");

module.exports = async function (req, res, next) {
  const token = req.header("x-auth-token");
  if (!token) return res.status(401).json({ error: "Unauthorized" });

  try {
    const decoded = jwt.verify(token, config.jwt.JWT_WEBTOKEN);
    req.auth = decoded;
    next();
  } catch (ex) {
    res.status(401).json({ error: "Unauthorized" });
  }
};
