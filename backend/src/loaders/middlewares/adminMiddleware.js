const jwt = require("jsonwebtoken");
const config = require("../../config");
const messages = require("../../services/API Messages/messages/messages");
const { Admin } = require("../../models/Admin");

module.exports = async function (req, res, next) {
  const token = req.header("x-auth-token");
  if (!token)
    return res.status(401).send(messages.authorization.admin.accessDenied);

  try {
    const decoded = jwt.verify(token, config.get("jwtPrivateKey"));
    const admin = await Admin.findById(decoded._id);
    if (!admin) {
      res
        .status(401)
        .json({ error: messages.authorization.admin.accessDenied });
    }
    next();
  } catch (ex) {
    res.status(401).json({ error: messages.authorization.admin.accessDenied });
  }
};
