module.exports = (data) => {
  amountToBeCut = (data.price * data.discount) / 100;

  return data.price - amountToBeCut;
};
