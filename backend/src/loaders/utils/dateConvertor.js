const moment = require("moment-jalaali");

module.exports = {
  J2M: (date, type) => {
    if (type === 1) {
      if (!moment(date, "jYYYY/jMM/jDD").isValid()) {
        return { date: null, error: "فرمت تاریخ اشتباه است" };
      }

      var gDate = moment(date, "jYYYY/jM/jD");
      gDate.format("jYYYY/jM/jD [is] YYYY-M-D");

      gDate = gDate.format("YYYY-MM-DD");

      return { convertedDate: gDate, error: null };
    }

    if (!moment(date, "YYYY-MM-DD").isValid()) {
      return { date: null, error: "فرمت تاریخ اشتباه است" };
    }

    var jDate = moment(date, "YYYY-M-D");
    jDate.format("YYYY-M-D [is] jYYYY/jM/jD");

    jDate = jDate.format("jYYYY/jM/jD");

    return { convertedDate: jDate, error: null };
  },
  Iso2Normal: (date) => {
    var messageDate = {
      year: new Date(date).getFullYear(),
      month: Number(date.slice(date.indexOf("-") + 1, date.lastIndexOf("-"))),
      day: new Date(date).getDate(),
    };

    return `${messageDate.year}-${messageDate.month}-${messageDate.day}`;
  },
};
