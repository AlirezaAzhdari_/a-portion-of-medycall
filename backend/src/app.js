const { startup } = require("./loaders/express");

var server;

async function app() {
  server = await startup();
}

app();

module.exports = server;
