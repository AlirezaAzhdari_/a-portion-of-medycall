module.exports = {
  createProfile: require("./profile/createProfile"),
  getAProfile: require("./profile/getAProfile"),
  updateProfile: require("./profile/updateProfile"),
};
