const { Profile } = require("../../../models/Profile");

module.exports = async (data) => {
  const newProfile = new Profile({
    firstName: data.firstName ? data.firstName : "",
    lastName: data.lastName ? data.lastName : "",
    phoneNumber: data.phoneNumber,
    biography: data.biography ? data.biography : "",
    avatarUrl: data.avatarUrl ? data.avatarUrl : "",
    gender: data.gender ? data.gender : "",
    age: data.age ? data.age : "",
    platform: data.platform ? data.platform : "",
  });

  const profile = await newProfile.save();

  return { profile };
};
