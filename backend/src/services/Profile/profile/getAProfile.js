const { Profile } = require("../../../models/Profile");
const apiMessages = require("../../API Messages");

module.exports = {
  byID: async (profile_id) => {
    const profile = await Profile.findById(profile_id);

    if (!profile) {
      return {
        profile: null,
        error: apiMessages.messages.db.profileNotFound,
      };
    }

    return { profile, error: null };
  },
  byName: async (data) => {
    const profile = await Profile.findOne({
      firstName: data.firstName,
      lastName: data.lastName,
    });

    if (!profile) {
      return {
        profile: null,
        error: apiMessages.messages.db.profileNotFound,
      };
    }

    return { profile, error: null };
  },
  byPhoneNumber: async (phoneNumber) => {
    const profile = await Profile.findOne({ phoneNumber });
    if (!profile) {
      return {
        _profile: null,
        error: apiMessages.messages.db.profileNotFound,
      };
    }

    return { _profile: profile, error: null };
  },
};
