const { Profile } = require("../../../models/Profile");
const upload = require("../../Cloudinary/cloudinary/upload");
const apiMessages = require("../../API Messages");
const fs = require("fs");
const emitter = require("../../Events/emitter");

module.exports = {
  updateNotifToken: async (data) => {
    const profile = await Profile.findByIdAndUpdate(
      data.profile_id,
      { notificationToken: data.notificationToken },
      { new: true }
    );

    if (profile) {
      return { profile, error: null };
    }

    return { profile: null, error: apiMessages.messages.db.profileNotFound };
  },
  updateProfilePicture: async (data, target) => {
    let multiple = async (tempFilePath) => await upload(tempFilePath);
    var newPath, profile;
    for (const img of [data.image]) {
      const { tempFilePath } = img;
      newPath = await multiple(tempFilePath);
      profile = await Profile.findByIdAndUpdate(
        data.profile_id,
        { avatarUrl: newPath },
        { new: true }
      );
      fs.unlinkSync(tempFilePath);
    }
    if (newPath) {
      return emitter.eventBus.sendEvent("uploadProfilePicDone", {
        profile: profile,
        target,
      });
    }
    const error = "عکس پروفایل آپلود نشد";
    return emitter.eventBus.sendEvent("uploadProfilePicError", {
      error,
      target,
    });
  },
  updatePatientProfile: async (data) => {
    const newProfile = await Profile.findByIdAndUpdate(
      data.profile_id,
      data.update,
      { new: true }
    );

    if (!newProfile) {
      return { profile: null, error: "پروفایلی برای این بیمار یافت نشد" };
    }

    return { profile: newProfile, error: null };
  },
};
