const mongoose = require("mongoose");
const { logger } = require("../../../logger/logger");
const config = require("../../../config/index");

module.exports = async () => {
  try {
    const res = await mongoose.connect(`${config.database.mongo_uri}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log("connected to db");
    return res.connection.db;
  } catch (error) {
    logger.error(error.message);
    console.log(error.message);
  }
};
