const { Session } = require("../../../models/Session");
const getConversation = require("../../Conversation/conversation/getConversation");

module.exports = {
  byID: async (session_id) => {
    //Checking if the session is already exists
    const session = await Session.findOne({
      _id: session_id,
    });

    if (!session) {
      return { session: null, error: "Did not find such a session" };
    }

    return { session, error: null };
  },
  activeSessByPatient: async (data) => {
    var sessions = await Session.find({ patient: data.id });
    if (!sessions) {
      return {
        sessionsIDs: null,
        error: "Did not find any session for this patient",
      };
    }
    let sessionsIDs = sessions.map((session) => {
      if (session.status === "active") {
        return session._id;
      }
    });
    return { sessionsIDs, error: null };
  },
  byPatientAndDoctor: async (data) => {
    var session = await Session.findOne({
      patient: data.patient,
      doctor: data.doctor,
    });
    if (!session) {
      return { session: null, error: "Did not find such session" };
    }
    return { session, error: null };
  },
  allDoctorSessions: async (data) => {
    const sessions = await Session.find({ doctor: data.doctor_id });
    if (!sessions) {
      return {
        sessions: null,
        error: "مشاوره‌ای برای شما ثبت نشده است",
      };
    }

    return { sessions, error: null };
  },
  allPatientSessions: async (data) => {
    const sessions = await Session.find({ patient: data.patient_id });
    if (!sessions) {
      return {
        sessions: null,
        error: "مشاوره‌ای برای شما ثبت نشده است",
      };
    }

    return { sessions, error: null };
  },
};
