const axios = require("axios");
const firebaseAdmin = require("firebase-admin");
const { Profile } = require("../../../models/Profile");
const config = require("../../../config");
const kavenegar = require("../../Kavenegar");

const httpClient = axios.create({
  baseURL: config.kavenegar.BaseURL,
  timeout: 10000,
  headers: { Authorization: "Bearer " + config.kavenegar.API_TOKEN },
});

module.exports = async (data) => {
  const patientProfile = await Profile.findById(data.receptor);
  const doctorProfile = await Profile.findById(data.caller);

  if (!patientProfile || !doctorProfile) {
    return { error: "Invalid caller or receptor", status: 500 };
  }

  const payload = {
    caller: {
      username: data.caller,
      displayName: `${doctorProfile.firstName} ${doctorProfile.lastName}`,
      platform: doctorProfile.platform,
    },
    receptor: {
      username: data.receptor,
      displayName:
        patientProfile.firstName && patientProfile.lastName
          ? `${patientProfile.firstName} ${patientProfile.lastName}`
          : "بیمار",
      platform: patientProfile.platform,
    },
  };

  httpClient
    .post("calls", payload)
    .then((response) => {
      const _data = response.data;
      console.log("Response of kavenegar :", _data);
      const notification = patientProfile.notificationToken;
      if (receptor.platform === "android" || receptor.platform === "web") {
        const message = {
          data: {
            action: "call",
            payload: JSON.stringify({
              callId: _data.id,
              accessToken: data.caller.accessToken,
            }),
          },
          token: notification,
        };

        firebaseAdmin
          .messaging()
          .send(message)
          .then((response) => {
            console.log("Firebase push notification sent message:", response);
            return {
              response: {
                callId: _data.id,
                accessToken: _data.caller.accessToken,
              },
              error: null,
            };
          })
          .catch((error) => {
            console.log("Firebase error sending message:", error);
            return { response: null, error };
          });
      }
    })
    .catch((error) => {
      return { response: null, error };
    });
};
