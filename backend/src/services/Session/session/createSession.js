const { Session } = require("../../../models/Session");

const conversations = require("../../Conversation/index");

module.exports = async (data) => {
  var newSesssion = new Session({
    messagesCount: 2,
    patient: data.patient_id,
    doctor: data.doctor_id,
    payment: data.payment_id,
    date: data.session.date,
    tag: {
      startHour: data.session.tag.startHour,
      startMinute: data.session.tag.startMinute,
      duration: data.session.tag.duration,
    },
    patientInitialRecord: {
      age: data.session.patientInitialRecord.age,
      description: data.session.patientInitialRecord.description,
      gender: data.session.patientInitialRecord.gender,
    },
    status: "pending",
  });

  var session = await newSesssion.save();
  data.session.id = session._id;
  await conversations.createConversation(data);

  return { session };
};
