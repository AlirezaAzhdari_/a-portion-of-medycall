const { Doctor } = require("../../../models/Doctor");
const { Schedule } = require("../../../models/Schedule");
const { Patient } = require("../../../models/Patient");
const { Payment } = require("../../../models/Payment");
const { Profile } = require("../../../models/Profile");
const { agenda } = require("../../../jobs");

const sessions = require("../../Session/index");
const kavenegar = require("../../Kavenegar");

module.exports = async (data) => {
  var _data = {
    doctor_id: data.doctor_id,
    patient_id: data.patient_id,
    payment_id: data.payment_id,
    doctorType: data.doctorType,
    session: {
      date: data.session.date,
      tag: {
        startHour: data.session.tag.start,
        endHour: data.session.tag.end,
        startMinute: 0,
        duration: 20,
      },
      patientInitialRecord: {
        age: data.session.patientInitialRecord.age,
        description: data.session.patientInitialRecord.description,
        gender: data.session.patientInitialRecord.gender,
      },
    },
  };

  const doctor = await Doctor.findById(_data.doctor_id);
  const schedule = await Schedule.findOne({ doctor: _data.doctor_id });
  const patient = await Patient.findById(_data.patient_id);
  //const payment = await Payment.findById(_data.payment_id);
  const payment = new Payment();
  _data.payment_id = payment._id;

  //payment checking later
  if (!doctor || !patient || !schedule) {
    return { result: null, error: "عدم موفقیت" };
  }

  if (
    doctor.categoryName === "روانپزشکی" ||
    doctor.categoryName === "روانشناسی"
  )
    _data.doctorType = 1;

  var convertedDate = new Date(_data.session.date).toISOString().trim();
  _data.session.date = convertedDate;

  var messageDate = {
    year: new Date(_data.session.date).getFullYear(),
    month: Number(
      _data.session.date.slice(
        _data.session.date.indexOf("-") + 1,
        _data.session.date.lastIndexOf("-")
      )
    ),
    day: new Date(_data.session.date).getDate(),
  };
  _data.session.messageDate = `${messageDate.year}-${messageDate.month}-${messageDate.day}`;

  var day = schedule.schedules.filter(
    (day) => new Date(day.date).toISOString().trim() == _data.session.date
  );
  day = day[0];

  if (!day) return { result: null, error: "تاریخ اشتباه" };
  var dayIndex = schedule.schedules.indexOf(day);

  var tag = day.tags.filter(
    (tag) => tag.startHour == _data.session.tag.startHour
  );
  tag = tag[0];
  var tagIndex = day.tags.indexOf(tag);

  if (!tag) {
    return { result: null, error: "بازه اشتباه" };
  }
  if (
    (tag.position >= 3 && _data.doctorType === 0) ||
    (tag.position >= 1 && _data.doctorType === 1)
  ) {
    return { result: null, error: "عدم ظرفیت" };
  }

  if (_data.doctorType === 0) {
    _data.session.tag.duration = 20;
  } else {
    _data.session.tag.duration = 60;
  }

  _data.session.tag.startMinute = _data.session.tag.duration * tag.position;

  const { session } = await sessions.createSession(_data);

  schedule.schedules[dayIndex].tags[tagIndex].position++;
  await schedule.save();

  const doctorProfile = await Profile.findById(doctor.profile);
  const patientProfile = await Profile.findById(patient.profile);

  const { patientSMS, doctorSMS } = kavenegar.formatSchedulingSMS({
    patient: {
      firstName: patientProfile.firstName ? patientProfile.firstName : "کاربر",
      lastName: patientProfile.lastName ? patientProfile.lastName : "",
      gender: patientProfile.gender,
    },
    doctor: {
      firstName: doctorProfile.firstName,
      lastName: doctorProfile.lastName,
      gender: doctorProfile.gender,
    },
    date: session.date,
    tag: {
      hour: session.tag.startHour,
      minute: session.tag.startMinute,
      duration: session.tag.duration,
    },
  });

  //patient SMS
  await agenda.schedule("2 minutes", "sendSchedulingSMS", {
    message: patientSMS,
    phoneNumber: patientProfile.phoneNumber,
  });

  //doctor SMS
  await agenda.schedule("2 minutes", "sendSchedulingSMS", {
    message: doctorSMS,
    phoneNumber: doctorProfile.phoneNumber,
  });

  return { result: { session }, error: null };
};
