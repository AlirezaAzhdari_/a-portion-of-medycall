const jwt = require("jsonwebtoken");
const { Patient } = require("../../../models/Patient");
const { Profile } = require("../../../models/Profile");
const config = require("../../../config");
const apiMessages = require("../../API Messages");
const profiles = require("../../Profile");
const kavenegar = require("../../Kavenegar");

module.exports = {
  first: async (data) => {
    const code = kavenegar.verificationCode();

    const _data = {
      phoneNumber: data.phoneNumber,
      code: code,
    };

    const response = await kavenegar.sendSignUpSMS.sendSMS(_data);
    if (response.result !== "Ok") {
      return {
        error: apiMessages.messages.verification.error,
        token: null,
      };
    }

    const token = jwt.sign({ code }, config.jwt.VERIF_SECRET, {
      expiresIn: 90,
    });

    return {
      token: token,
      error: null,
    };
  },
  second: async (data) => {
    const { token, code } = data;

    if (!token || !code) {
      return {
        error: apiMessages.messages.verification.emptyParams,
        patient: null,
        authToken: null,
      };
    }

    let decoded;
    try {
      decoded = jwt.verify(token, config.jwt.VERIF_SECRET);
    } catch (ex) {
      return {
        error: ex.message,
        patient: null,
        authToken: null,
      };
    }

    if (code != decoded.code) {
      return {
        error: apiMessages.messages.verification.wrongCode,
        patient: null,
        authToken: null,
      };
    }

    var { _profile } = await profiles.getAProfile.byPhoneNumber(
      data.phoneNumber
    );
    if (_profile) {
      const patient = await Patient.findOne({ profile: _profile._id });

      if (patient) {
        const authToken = patient.generateAuthToken();

        return { patient, authToken, error: null };
      }

      await Patient.deleteOne({ profile: _profile._id });
      await Profile.deleteOne({ phoneNumber: data.phoneNumber });
    }

    const newProfile = new Profile({
      phoneNumber: data.phoneNumber,
    });

    const profile = await newProfile.save();

    const newPatient = new Patient({
      profile: profile._id,
    });
    const patient = await newPatient.save();
    const authToken = patient.generateAuthToken();

    return { patient: patient, authToken: authToken, error: null };
  },
};
