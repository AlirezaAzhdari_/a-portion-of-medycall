module.exports = {
  createPatient: require("./patient/createPatient"),
  scheduleSession: require("./patient/scheduleSession"),
};
