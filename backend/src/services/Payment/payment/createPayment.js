const { Payment } = require("../../../models/Payment");
const config = require("../../../config");

module.exports = async (data) => {
  const newPayment = new Payment({
    patient: data.patient_id,
    couponID: data.coupon_id,
    amount: data.amount,
    authority: data.authority,
  });

  const payment = await newPayment.save();
  return { payment };
};
