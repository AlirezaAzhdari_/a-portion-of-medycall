const ZarinpalCheckout = require("zarinpal-checkout");
const config = require("../../../config");

var zarinpal = ZarinpalCheckout.create(config.zarinpal.merchant_Id, true);
module.exports.zarinpal = zarinpal;
