module.exports = {
  createPayment: require("./payment/createPayment"),
  initializeZarinpal: require("./payment/initializeZarinpal"),
};
