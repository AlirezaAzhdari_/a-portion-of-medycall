const { Schedule } = require("../../../models/Schedule");

module.exports = {
  getAll: async () => {
    const schedules = await Schedule.find();
    if (!schedules) {
      return { result: null, error: "هیچ زمان‌بندی‌ای یافت نشد" };
    }
    return { result: schedules, error: null };
  },
  byDoctorId: async (data) => {
    const schedule = await Schedule.findOne({ doctor: data.doctor_id });
    if (!schedule) {
      const newSchedule = new Schedule({
        doctor: data.doctor_id,
      });
      await newSchedule.save();

      return { result: newSchedule, error: null };
    }
    return { result: schedule, error: null };
  },
  byID: async (schedule_id) => {
    const schedule = await Schedule.findById(schedule_id);

    if (!schedule)
      return { result: null, error: "زمان‌بندی برای شما یافت نشد" };
    return { result: schedule, error: null };
  },
};
