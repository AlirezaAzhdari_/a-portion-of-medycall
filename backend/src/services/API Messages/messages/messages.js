module.exports = {
  asyncErro: { failed: "Something failed" },
  authentication: {
    phoneEmpty: ".لطفا شماره تلفن خود را وارد کنید",
    passwordEmpty: ".لطفا رمز عبور خود را وارد کنید",
    phoneNumberIsWrong: ".شماره تلفن اشتباه است",
    passwordIsWrong: ".رمز عبور اشتباه است",
    somethingIsWrong: ".شماره یا رمز عبور اشتباه است",
    userNotFound: ".کاربری با این مشخصات پیدا نشد",
    adminNotFound: ".ادمینی با این مشخصات پیدا نشد",
  },
  cloudinary: {
    image: {
      success: "تصویر آپلود شد",
      failed: "تصویر آپلود نشد. لطفا دوباره تلاش کنید",
    },
    video: {
      success: "ویدئو آپلود شد",
      failed: "ویدئو آپلود نشد. لطفا دوباره تلاش کنید",
    },
    file: {
      success: "فایل آپلود شد",
      failed: "فایل آپلود نشد. لطفا دوباره تلاش کنید",
    },
  },
  db: {
    patientNotFound: "بیماری با این مشخصات یافت نشد",
    profileNotFound: "پروفایلی با این مشخصات یافت نشد",
    doctorNotFound: "پزشکی با این مشخصات سافت نشد",
  },
  verification: {
    error: "پیامک ارسال نشد. لطفا دوباره تلاش کنید",
    success: "پیام با موفقیت ارسال شد",
    emptyParams: "توکن یا کد وجود ندارد",
    wrongCode: ".کد ارسالی اشتباه است",
    codeExpired: "کد منقضی شده است",
    patientExists: "بیماری با این مشخصات وجود دارد",
    patientRecovery: "Patient Recovered.",
  },
  category: {
    nameEmpty: "لطفا نام را وارد کنید",
    descriptionEmpty: "لطفا توضیحات را وارد کنید",
    priceEmpty: "لطفا قیمت را وارد کنید",
    somethingIsWrong: "فرمت بعضی از فیلد ها اشتباه است",
  },
  authorization: {
    admin: {
      accessDenied: "دسترسی ندارید",
    },
  },
  doctors: {
    notSavingSchedule: "زمان بندی ذخیره نشد. لطفا دوباره تلاش کنید",
  },
};
