module.exports = {
  sendSignUpSMS: require("./kavenegar/sendSignUpSMS"),
  verificationCode: require("./kavenegar/verificationCode"),
  sendNotification: require("./kavenegar/sendNotification"),
  schedulingSMS: require("./kavenegar/scheduling/schedulingSMS"),
  formatSchedulingSMS: require("./kavenegar/scheduling/formatSchedulingSMS"),
};
