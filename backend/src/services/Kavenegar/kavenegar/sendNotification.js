const firebaseAdmin = require("firebase-admin");

module.exports = {
  send: async (data) => {
    var promise = new Promise((resolve, reject) => {
      if (data.platform === "android" || data.platform === "web") {
        const message = {
          data: {
            action: "call",
            payload: JSON.stringify(data.payload),
          },
          token: data.token,
        };

        firebaseAdmin
          .messaging()
          .send(message)
          .then((response) => {
            console.log("Firebase push notification sent message:", response);
            resolve({ response, error: null });
          })
          .catch((error) => {
            console.log("Firebase error sending message:", error);
            reject({ response: null, error });
          });
      }
    });

    return promise;
  },
};
