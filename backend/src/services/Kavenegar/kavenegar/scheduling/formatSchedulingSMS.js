module.exports = (data) => {
  var patientSMS = ``;
  data.patient.gender === "female" ? (patientSMS = ``) : (patientSMS = ``);

  var doctorSMS = ``;
  data.doctor.gender === "female" ? (doctorSMS = ``) : (doctorSMS = ``);

  return { patientSMS, doctorSMS };
};
