const kavenegar = require("kavenegar");
const config = require("../../../config");

const sender = config.kavenegar.Sender;

var api = kavenegar.KavenegarApi({
  apikey: config.kavenegar.API_KEY,
});

module.exports = {
  sendSMS: async (data) => {
    return new Promise((resolve, reject) => {
      api.Send(
        {
          message: data.code,
          sender,
          receptor: data.phoneNumber,
        },
        (response, status) => {
          if (status === 200) {
            resolve({ result: "Ok" });
          } else {
            reject({ result: "Not Ok" });
          }
        }
      );
    });
  },
};
