module.exports = () => {
  const code = Math.floor(Math.random() * 9999 + 1000);
  var codeString = code.toString();
  if (codeString.length > 4) {
    return Number(codeString.slice(0, 4));
  }
  return code;
};
