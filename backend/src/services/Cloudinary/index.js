module.exports = {
  initCloudinary: require("./cloudinary/initCloudinary"),
  uploadImage: require("./cloudinary/uploadImage"),
  upload: require("./cloudinary/upload"),
};
