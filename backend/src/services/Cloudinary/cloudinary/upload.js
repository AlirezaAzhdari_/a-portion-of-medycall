const cloudinary = require("cloudinary").v2;
const Q = require("q");

module.exports = (image) => {
  return new Q.Promise((resolve, reject) => {
    cloudinary.uploader.upload(image, (err, res) => {
      if (err) {
        console.log("cloudinary err:", err);
        reject(err);
      } else {
        console.log("cloudinary res:", res.secure_url);
        return resolve(res.secure_url);
      }
    });
  });
};
