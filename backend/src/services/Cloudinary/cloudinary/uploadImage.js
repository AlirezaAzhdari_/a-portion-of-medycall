const cloudinary = require("cloudinary").v2;
const { Conversation } = require("../../../models/Conversation");

module.exports = async (data) => {
  const conversation = await Conversation.findOne({ session: data.session });

  if (conversation) {
    const result = await cloudinary.uploader.upload(data.image);
    const image = {
      url: result.secure_url,
      name: result.original_filename,
    };

    await conversation.updateOne({ $push: { images: image } });
    return { success: "true", error: null };
  }

  return { success: "false", error: "there is not such a conversation" };
};
