const config = require("../../../config");

module.exports = (cloudinary) => {
  cloudinary.config({
    cloud_name: config.cloudinary.CLOUD_NAME,
    api_key: config.cloudinary.API_KEY,
    api_secret: config.cloudinary.API_SECRET,
  });
};
