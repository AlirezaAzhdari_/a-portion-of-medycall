module.exports = {
  createDoctor: require("./doctor/createDoctor"),
  getDoctor: require("./doctor/getDoctor"),
  scheduling: require("./doctor/scheduling"),
  editDoctor: require("./doctor/editDoctor"),
  allInfo: require("./doctor/allInfo"),
  allInfoSearch: require("./doctor/allInfoSearch"),
};
