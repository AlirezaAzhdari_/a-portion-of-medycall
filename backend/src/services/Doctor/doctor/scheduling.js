const { Doctor } = require("../../../models/Doctor");
const { Schedule } = require("../../../models/Schedule");
const messages = require("../../API Messages/messages/messages");

module.exports = async (data) => {
  const doctor = await Doctor.findById(data.doctor_id);
  const schedule = await Schedule.findOne({ doctor: data.doctor_id });

  if (!doctor) {
    return { error: messages.doctors.notSavingSchedule };
  }

  if (!schedule) {
    const newSchedule = new Schedule({
      doctor: data.doctor_id,
      schedules: data.schedule,
    });
    await newSchedule.save();
    return { error: null };
  }

  schedule.schedules.length = 0;
  schedule.schedules = data.schedule.slice();

  await schedule.save();
  return { error: null };
};
