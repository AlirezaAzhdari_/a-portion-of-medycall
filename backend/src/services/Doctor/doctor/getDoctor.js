const { Doctor } = require("../../../models/Doctor");

module.exports = {
  byCategory: async (data) => {
    const doctors = await Doctor.find({ categoryName: data.categoryName });

    if (!doctors) {
      return { doctors: null, error: ".پزشکی در این تخصص یافت نشد" };
    }

    return { doctors, error: null };
  },
};
