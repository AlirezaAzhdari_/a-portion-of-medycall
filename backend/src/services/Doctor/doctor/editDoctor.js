const { Schedule } = require("../../../models/Schedule");

module.exports = {
  resetSchedule: async (data) => {
    const update = { schedules: [] };
    const schedule = await Schedule.findOneAndUpdate(
      { doctor: data.doctor_id },
      update,
      { new: true }
    );
    if (!schedule) {
      return { message: null, error: "شما قبلا زمان‌بندی نکرده‌اید" };
    }

    return { message: "برنامه زمان‌بندی شما با موفقیت پاک شد", error: null };
  },
};
