const { Doctor } = require("../../../models/Doctor");
const { Schedule } = require("../../../models/Schedule");

module.exports = async (data) => {
  var doctors = await Doctor.find();
  var fullDoctors = [];

  if (doctors.length === 0) {
    return { fullDoctors: null, error: ".پزشکی یافت نشد" };
  }

  if (data.searchKey) {
    var doctorsByName = doctors.filter(
      (doctor) =>
        doctor.name.includes(data.searchKey) ||
        data.searchKey.includes(doctor.name)
    );

    const searchString = data.searchKey.split(" ");
    var _doctors = [];

    for (let i = 0; i < doctors.length; i++) {
      for (let j = 0; j < searchString.length; j++) {
        if (
          (doctors[i].categoryName.includes(searchString[j]) &&
            searchString[j].length > 1) ||
          searchString[j].includes(doctors[i].categoryName)
        )
          _doctors.push(doctors[i]);
      }
    }

    doctors.length = 0;
    doctors = [...new Set(_doctors.concat(doctorsByName))];
  } else {
    if (data.categoryName) {
      doctors = doctors.filter(
        (doctor) =>
          doctor.categoryName === data.categoryName ||
          doctor.categoryName.includes(data.categoryName) ||
          data.categoryName.includes(doctor.categoryName)
      );
    }
  }

  if (doctors.length === 0) {
    return { fullDoctors: null, error: ".پزشکی یافت نشد" };
  }

  for (let i = 0; i < doctors.length; i++) {
    const schedule = await Schedule.findOne({ doctor: doctors[i]._id });

    const newDoctor = {
      doctor: doctors[i],
      schedule,
    };

    fullDoctors.push(newDoctor);
  }

  return { fullDoctors, error: null };
};
