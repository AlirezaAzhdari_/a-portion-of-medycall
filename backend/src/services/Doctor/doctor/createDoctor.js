const { Doctor } = require("../../../models/Doctor");
const bcrypt = require("bcrypt");

module.exports = async (data) => {
  const doctor = await Doctor.findOne({
    phoneNumber: data.phoneNumber,
  });

  if (doctor) {
    return {
      doctor: null,
      error: "Doctor Already exists.",
    };
  }

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(data.password, salt);
  data.password = hashedPassword;

  var newDoctor = new Doctor({
    name: data.name,
    phoneNumber: data.phoneNumber,
    biography: data.biography,
    avatarUrl: data.avatarUrl,
    videoUrl: data.videoUrl,
    gender: data.gender,
    age: data.age,
    doctorType: data.doctorType,
    platform: data.platform,
    categoryName: data.categoryName,
    password: data.password,
    nezamPezeshki: data.nezamPezeshki,
  });

  const _doctor = await newDoctor.save();

  return { doctor: _doctor, error: null };
};
