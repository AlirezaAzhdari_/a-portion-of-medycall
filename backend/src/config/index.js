const dotenv = require("dotenv");
process.env.NODE_ENV = process.env.NODE_ENV || "development";
var envFound;

function setENVs() {
  if (envFound === undefined) {
    envFound = dotenv.config();
  }

  if (!envFound) {
    //this error should crash the whole process
    throw new Error("couldn't find .env file");
  }
  return {
    port: parseInt(process.env.PORT, 10),
    logs: {
      level: process.env.LOG_LEVEL || "silly",
    },

    database: {
      username: process.env.DBusername,
      password: process.env.DBpassword,
      address: process.env.DBaddress,
      mongo_uri: process.env.MONGO_URI,
    },
    api: {
      prefix: process.env.PREFIX,
    },
    cloudinary: {
      api_key: process.env.API_KEY_CLOUDINARY,
      api_secret: process.env.API_SECRET,
      cloud_name: process.env.CLOUD_NAME,
      cloudinary_url: `cloudinary://${process.env.API_KEY}:${process.env.API_SECRET}@${process.env.CLOUD_NAME}`,
    },
    jwt: {
      JWTWebToken: process.env.JWT_WEBTOKEN,
      verifSecret: process.env.VERIF_SECRET,
    },
    kavenegar: {
      API_KEY: process.env.API_KEY_KAVENEGAR,
      Sender: process.env.SENDER,
      API_TOKEN: process.env.API_TOKEN,
      BaseURL: process.env.BASE_URL,
    },
    firebase: {
      apiKey: process.env.API_KEY_FIREBASE,
      authDomain: process.env.AUTH_DOMAIN,
      databaseURL: process.env.DATABASE_URL,
      projectId: process.env.PROJECT_ID,
      storageBucket: process.env.STORAGE_BUCKET,
      messagingSenderId: process.env.MESSAGING_SENDER_ID,
      appId: process.env.APP_ID,
      measurementId: process.env.MEASUREMENT_ID,
    },
    zarinpal: {
      merchant_Id: process.env.MERCHANT_ID,
      payment_Description: process.env.PAYMENT_DESCRIPTION,
    },
  };
}

module.exports = {
  port: setENVs().port,

  logs: {
    level: setENVs().logs.level,
  },

  database: {
    username: setENVs().database.username,
    password: setENVs().database.password,
    address: setENVs().database.address,
    mongo_uri: setENVs().database.mongo_uri,
  },
  api: {
    prefix: setENVs().api.prefix,
  },
  cloudinary: {
    API_KEY: setENVs().cloudinary.api_key,
    API_SECRET: setENVs().cloudinary.api_secret,
    CLOUD_NAME: setENVs().cloudinary.cloud_name,
    CLOUDINARY_URL: setENVs().cloudinary.cloudinary_url,
  },
  jwt: {
    JWT_WEBTOKEN: setENVs().jwt.JWTWebToken,
    VERIF_SECRET: setENVs().jwt.verifSecret,
  },
  kavenegar: {
    API_KEY: setENVs().kavenegar.API_KEY,
    Sender: setENVs().kavenegar.Sender,
    API_TOKEN: setENVs().kavenegar.API_TOKEN,
    BaseURL: setENVs().kavenegar.BaseURL,
  },
  firebase: {
    apiKey: setENVs().firebase.apiKey,
    authDomain: setENVs().firebase.authDomain,
    databaseURL: setENVs().firebase.databaseURL,
    projectId: setENVs().firebase.projectId,
    storageBucket: setENVs().firebase.storageBucket,
    messagingSenderId: setENVs().firebase.messagingSenderId,
    appId: setENVs().firebase.appId,
    measurementId: setENVs().firebase.measurementId,
  },
  zarinpal: {
    merchant_Id: setENVs().zarinpal.merchant_Id,
    payment_Description: setENVs().zarinpal.payment_Description,
  },
};
